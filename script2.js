const form = document.querySelector('.password-form');
const errorMessage = document.querySelector('.error');
const inputParent = form.querySelectorAll('.input-wrapper');
const inputItem = document.querySelectorAll('.input-password');
let btnSubmit = form.querySelector('.btn-submit');

inputParent.forEach(() => addEventListener('click', changeViewPassword));
btnSubmit = addEventListener('click', comparePassword);

function changeViewPassword(e) {
  const iconEye = e.target;
  if (iconEye.nodeName === 'I' && iconEye.classList.contains('icon-password')) {
    const inputPassword = iconEye.parentElement.querySelector('.input-password');
    iconEye.classList.toggle('fa-eye-slash');
    iconEye.classList.toggle('fa-eye');
    inputPassword.type = (inputPassword.type === 'password') ? 'text' : 'password';
  }
}

function comparePassword(e) {
  const senderSubmit = e.target;
  if (senderSubmit.nodeName === 'BUTTON' && senderSubmit.classList.contains('btn-submit')) {
    e.preventDefault();
    if (inputItem[0].value === '') {
      errorMessage.innerText = 'Нужно ввести значения';
      errorMessage.style.color = 'red';
    }
    else if (inputItem[0].value === inputItem[1].value) {
      errorMessage.innerText = '';
      // console.log(`You are welcome`);
      alert(`You are welcome`);
    } else {
      errorMessage.innerText = 'Нужно ввести одинаковые значения';
      errorMessage.style.color = 'red';
    }
  }

}